package ru.zhurkin.sberlibrary.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.zhurkin.sberlibrary.dao.BookDAO;
import ru.zhurkin.sberlibrary.dao.UserDAO;
import ru.zhurkin.sberlibrary.dto.UserDTO;
import ru.zhurkin.sberlibrary.model.Book;
import ru.zhurkin.sberlibrary.model.Client;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserDAO userDAO;
    private final BookDAO bookDAO;

    @GetMapping("/{telephoneNumber}")
    public ResponseEntity<List<Book>> getUsersBooksInfoByTelephone(@PathVariable String telephoneNumber) {

        Client client = userDAO.getUserByTelephone(telephoneNumber);
        List<String> bookTitles = client.getBooks();
        List<Book> books = bookTitles.stream()
                .map(bookDAO::getBookByTitle)
                .toList();
        return ResponseEntity.ok(books);
    }

    @GetMapping
    public ResponseEntity<List<Client>> getAllUsers() {

        return ResponseEntity.ok(userDAO.getAllClients());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String saveClient(@RequestBody UserDTO dto) {

        boolean saved =  userDAO.saveUser(dto.id(),
                dto.surname(),
                dto.name(),
                dto.birthDate(),
                dto.telephoneNumber(),
                dto.books());

        String answer = saved ? "Client was saved" : "Client wasn't saved";
        return answer;
    }
}
