package ru.zhurkin.sberlibrary.dao;

import ru.zhurkin.sberlibrary.model.Book;

import java.util.List;

public interface BookDAO {

    Book getBookByTitle(String title);
    List<Book> getAllBooks();
}
