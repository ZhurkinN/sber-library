package ru.zhurkin.sberlibrary.dao;

import ru.zhurkin.sberlibrary.model.Client;

import java.sql.Date;
import java.util.List;

public interface UserDAO {

    boolean saveUser(Long id,
                     String surname,
                     String name,
                     Date birthDate,
                     String telephoneNumber,
                     List<String> books);

    List<Client> getAllClients();

    Client getUserByTelephone(String telephoneNumber);
}
