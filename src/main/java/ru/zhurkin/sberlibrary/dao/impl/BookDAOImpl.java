package ru.zhurkin.sberlibrary.dao.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.zhurkin.sberlibrary.dao.BookDAO;
import ru.zhurkin.sberlibrary.mapper.BookMapper;
import ru.zhurkin.sberlibrary.model.Book;

import java.util.List;

import static ru.zhurkin.sberlibrary.constants.QueriesKeeper.GET_ALL_BOOKS_QUERY;
import static ru.zhurkin.sberlibrary.constants.QueriesKeeper.GET_BOOK_BY_TITLE_QUERY;

@Repository
@RequiredArgsConstructor
public class BookDAOImpl implements BookDAO {

    private final JdbcTemplate jdbcTemplate;
    @Override
    public Book getBookByTitle(String title) {
        return jdbcTemplate.queryForObject(GET_BOOK_BY_TITLE_QUERY, new Object[] {title}, new BookMapper());
    }

    @Override
    public List<Book> getAllBooks() {
        return jdbcTemplate.query(GET_ALL_BOOKS_QUERY, new BookMapper());
    }
}
