package ru.zhurkin.sberlibrary.dao.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.zhurkin.sberlibrary.dao.UserDAO;
import ru.zhurkin.sberlibrary.mapper.UserMapper;
import ru.zhurkin.sberlibrary.model.Client;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.List;

import static ru.zhurkin.sberlibrary.constants.QueriesKeeper.*;

@Repository
@Scope(scopeName = "prototype")
@RequiredArgsConstructor
public class UserDAOImpl implements UserDAO {

    private final JdbcTemplate jdbcTemplate;

    @Override
    @Transactional
    public boolean saveUser(Long id,
                            String surname,
                            String name,
                            Date birthDate,
                            String telephoneNumber,
                            List<String> books) {
        int rowsChanged = jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(INSERT_CLIENT_QUERY);
            ps.setObject(1, id);
            ps.setObject(2, surname);
            ps.setObject(3, name);
            ps.setObject(4, birthDate);
            ps.setObject(5, telephoneNumber);
            ps.setObject(6, con.createArrayOf("text", books.toArray()));
            return ps;
        });
        return rowsChanged > 0;
    }

    @Override
    public List<Client> getAllClients() {
        return jdbcTemplate.query(GET_ALL_CLIENTS_QUERY, new UserMapper());
    }

    @Override
    public Client getUserByTelephone(String telephoneNumber) {
        return jdbcTemplate.queryForObject(GET_CLIENT_BY_TELEPHONE_QUERY, new Object[] {telephoneNumber}, new UserMapper());
    }
}
