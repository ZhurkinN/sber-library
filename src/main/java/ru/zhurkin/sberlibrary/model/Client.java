package ru.zhurkin.sberlibrary.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.sql.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Accessors(chain = true)
public class Client {

    private Long id;
    private String surname;
    private String name;
    private Date birthDate;
    private String telephoneNumber;
    private List<String> books;
}
