package ru.zhurkin.sberlibrary.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@NoArgsConstructor
@Accessors(chain = true)
public class Book {

    private Long id;
    private String title;
    private String author;
    private String information;
}
