package ru.zhurkin.sberlibrary.dto;

import java.sql.Date;
import java.util.List;

public record UserDTO(Long id,
                      String surname,
                      String name,
                      Date birthDate,
                      String telephoneNumber,
                      List<String> books) {
}
