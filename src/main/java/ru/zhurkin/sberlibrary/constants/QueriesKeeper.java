package ru.zhurkin.sberlibrary.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class QueriesKeeper {

    public static final String GET_ALL_CLIENTS_QUERY = "" +
            "SELECT * " +
            "FROM client";
    public static final String INSERT_CLIENT_QUERY = "" +
            "INSERT INTO client (id, surname, name, birth_date, telephone_number, books) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    public static final String GET_CLIENT_BY_TELEPHONE_QUERY = "" +
            "SELECT * " +
            "FROM client " +
            "WHERE telephone_number = ?";
    public static final String GET_BOOK_BY_TITLE_QUERY = "" +
            "SELECT * " +
            "FROM book " +
            "WHERE title = ?";
    public static final String GET_ALL_BOOKS_QUERY = "" +
            "SELECT * " +
            "FROM book";
}
