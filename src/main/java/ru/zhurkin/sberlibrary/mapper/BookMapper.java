package ru.zhurkin.sberlibrary.mapper;

import org.springframework.jdbc.core.RowMapper;
import ru.zhurkin.sberlibrary.model.Book;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {

    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Book()
                .setId(rs.getLong("id"))
                .setTitle(rs.getString("title"))
                .setAuthor(rs.getString("author"))
                .setInformation(rs.getString("information"));
    }
}
