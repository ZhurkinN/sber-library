package ru.zhurkin.sberlibrary.mapper;

import org.springframework.jdbc.core.RowMapper;
import ru.zhurkin.sberlibrary.model.Client;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class UserMapper implements RowMapper<Client> {

    @Override
    public Client mapRow(ResultSet rs, int rowNum) throws SQLException {

        String[] bookTitles = (String[]) rs.getArray("books").getArray();

        return new Client()
                .setId(rs.getLong("id"))
                .setName(rs.getString("name"))
                .setSurname(rs.getString("surname"))
                .setBirthDate(rs.getDate("birth_date"))
                .setTelephoneNumber(rs.getString("telephone_number"))
                .setBooks(Arrays.stream(bookTitles).toList());
    }
}
