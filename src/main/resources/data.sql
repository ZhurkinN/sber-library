INSERT INTO client (id, surname, name, birth_date, telephone_number, books)
VALUES (1, 'Zhurkin', 'Nikita', now(), '89006002323', ARRAY ['Бойцовский клуб', 'Ведьмак', 'Песнь Льда и Пламени']);
INSERT INTO client (id, surname, name, birth_date, telephone_number, books)
VALUES (2, 'Zakharov', 'Ilya', now(), '89123531232', ARRAY ['Бойцовский клуб', 'Ведьмак']);

INSERT INTO book (id, title, author, information)
VALUES (1, 'Бойцовский клуб', 'Паланик', 'Отличная книга');
INSERT INTO book (id, title, author, information)
VALUES (2, 'Ведьмак', 'Сапковский', 'Захватывающий сюжет');
INSERT INTO book (id, title, author, information)
VALUES (3, 'Песнь Льда и Пламени', 'Мартин', 'Книга 18+');