CREATE TABLE IF NOT EXISTS client (
    id BIGINT NOT NULL PRIMARY KEY UNIQUE,
    surname VARCHAR(40),
    name VARCHAR(40),
    birth_date DATE,
    telephone_number varchar(20) UNIQUE,
    books varchar[]
);

CREATE TABLE IF NOT EXISTS book (
    id BIGINT NOT NULL PRIMARY KEY UNIQUE,
    title VARCHAR(40),
    author VARCHAR(40),
    information varchar(60)
)
