# sber-library

Simple solution for one of SberUniversity's tasks.

##Task's condition

1. Create a client table with fields:
    ● Last name
    ● Name
    ● Date of birth
    ● Phone
    ● Mail
    ● List of book titles from the library [“name", “name", ...]
2. Create a UserDao class and a Bean (prototype) of this class.
3. Next, we need to fill in users (i.e. we need to have a method in the code that allows us to add users to the database)
4. Write a method that accepts phone/mail (at your discretion), will get from UserDao is a list of the titles of this person's books. With this list, go to BookDAO and get all the information about these books